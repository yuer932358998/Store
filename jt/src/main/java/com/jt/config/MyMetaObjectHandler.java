package com.jt.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component  //可以加server注解，为了区分所以加Component注解
public class MyMetaObjectHandler implements MetaObjectHandler {
    //入库 新增时自动填充
    @Override
    public void insertFill(MetaObject metaObject) {
        Date date = new Date();//date表示新增或修改数据的时间
        this.setFieldValByName("created", date,metaObject);
        this.setFieldValByName("updated", date,metaObject);
    }

    //更新时自动填充
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updated",new Date(),metaObject);
    }
}
