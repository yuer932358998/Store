package com.jt.config;

import com.jt.vo.SysResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@ControllerAdvice + RsetController //全局异常的处理配置类的注解 拦截Controller层的专门注解 返回值为JSON
@RestControllerAdvice
public class MyExceptionConfig {
    //问题2：什么时候调用？
    @ExceptionHandler(RuntimeException.class)//处理运行时异常
    public Object handler(Exception exception){
        exception.printStackTrace();//将报错信息在控制台打印，否则有报错也看不到哪里出错
        return SysResult.fail();
    }

}
