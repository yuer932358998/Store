package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemCatSeriveImpl implements ItemCatService{
    @Autowired
    private ItemCatMapper itemCatMapper;



    /**
     * 问题分析:
     *      1.提高程序的效率  减少数据库交互的次数
     *      2.查询的方法最好单独的抽取
     * 问题: 如何有效的存储父子关系
     * 数据结构: Map<parentId,子级>
     * 说明:
     *      Map<0,List<ItemCat>>   map.get(0)
     *      Map<1,List<电子书刊,英文书籍>>
     *      Map<24,List<少儿,0-2岁....>>
     * @return
     */
    public Map<Integer,List<ItemCat>> getMap(){
        Map<Integer,List<ItemCat>> map = new HashMap<Integer,List<ItemCat>>();//1.封装一个Map集合
        List<ItemCat> list = itemCatMapper.selectList(null);//查询所有的数据信息
        for(ItemCat itemCat : list){    //遍历list集合，获取list的所有数据
            //规则: 判断map中是否有key
            // 没有key  该子级是第一个父级元素的孩子,应该声明父级并且将子级最为第一个子级保存
            // 有key    我找到父级的子级序列 将子级追加到序列中即可.
            if(map.containsKey(itemCat.getParentId())){//判断是否有父级
                map.get(itemCat.getParentId()).add(itemCat);//获取父级所有的已知子集,讲将自己作为子集加入到父级目录下
            }else{//如果没有父级
                List<ItemCat> initList = new ArrayList<>();//创建一个initList集合
                initList.add(itemCat);//将自己加入到initList集合里
                map.put(itemCat.getParentId(),initList);//将自己设置为父级
            }
        }
        return map;

    }

    @Override
    public List<ItemCat> findItemCatList(Integer type) {
        //获取数据封装后的结果
        Map<Integer,List<ItemCat>> map = getMap();
        if(type == 1){ //获取一级商品分类信息
            return map.get(0);
        }

        if(type == 2){

            return getLevel2(map);
        }

        return getLevel3(map);
    }

    @Override
    public void updateStatus(ItemCat itemCat) {
        itemCatMapper.updateById(itemCat);
    }

    @Override
    @Transactional
    public void saveItemCat(ItemCat itemCat) {
        itemCat.setStatus(true);
        itemCatMapper.insert(itemCat);
    }

    @Override
    @Transactional
    public void deleteItemCat(Integer id, Integer level) {
        if(level == 3){//如果是3级菜单
            itemCatMapper.deleteById(id);//则直接删除
        }
        if(level == 2){//如果是2级菜单
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("parent_id",id);
            itemCatMapper.delete(queryWrapper);//先删除2级的子集菜单
            itemCatMapper.deleteById(id);//再删除2级菜单
        }
        if(level == 1){
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("parent_id",id);
            List<Object> twoIdlist = itemCatMapper.selectObjs(queryWrapper);//获取主键信息，第一列信息
            for (Object twoId : twoIdlist) {//遍历2级菜单下子集的主键ID
                QueryWrapper queryWrapper2 = new QueryWrapper();
                queryWrapper2.eq("parent_id",twoId);
                itemCatMapper.delete(queryWrapper2);//根据2级菜单下的主键ID，删除了三级菜单
                Integer intTwoId = (Integer) twoId;
                itemCatMapper.deleteById(intTwoId);
            }

        }
        itemCatMapper.deleteById(id);


    }

    @Override
    @Transactional
    public void updateItemCat(ItemCat itemCat) {
        itemCatMapper.updateById(itemCat);
    }

    @Override
    public ItemCat getform(Integer id) {

        return itemCatMapper.selectById(id);


    }

    private List<ItemCat> getLevel3(Map<Integer, List<ItemCat>> map) {
        List<ItemCat> list = getLevel2(map);//获取二级商品分类信息 1级里面套二级
        for (ItemCat itemCat1 : list) {
            List<ItemCat> list2 = itemCat1.getChildren();
            //根据2级查询3级信息
            if(list2 == null) continue;
            for (ItemCat itemCat2 : list2) {
                List<ItemCat> list3 = map.get(itemCat2.getId());
                itemCat2.setChildren(list3);
            }
            itemCat1.setChildren(list2);
        }
        return list;
    }

    private List<ItemCat> getLevel2(Map<Integer, List<ItemCat>> map) {
        List<ItemCat> list = map.get(0);//获取1级商品分类信息
        for (ItemCat itemCat : list) {//封装2级菜单信息,遍历的是1级菜单
            List<ItemCat> twoList = map.get(itemCat.getId());//get一级的id才能得到二级
            itemCat.setChildren(twoList);//将二级菜单信息封装到一级菜单里面当做子集
        }
        return list;
    }
}
