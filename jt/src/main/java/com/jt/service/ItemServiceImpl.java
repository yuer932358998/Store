package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService{
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ItemDescMapper itemDescMapper;

    @Override
    public PageResult findItemByPage(PageResult pageResult) {
        IPage<Item> page =
                new Page<>(pageResult.getPageNum(), pageResult.getPageSize());
        QueryWrapper<Item> queryWrapper = new QueryWrapper<>();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"title",pageResult.getQuery());
        //经过程序分页,其中的数据全部获取
        page = itemMapper.selectPage(page,queryWrapper);
        long total = page.getTotal();
        List<Item> itemList = page.getRecords(); //MP获取分页结果
        return pageResult.setTotal(total).setRows(itemList);
    }

    /**
     * 完成两张表的入库操作
     * 1.item表
     * 2.item_desc表 要求id一致
     * @param itemVO
     */
    @Override
    @Transactional  //控制事务
    public void saveItem(ItemVO itemVO) {
        //获取商品对象
        Item item = itemVO.getItem();
        item.setStatus(true);//商品的默认状态为true
        //MP已经实现了主键的自动回显 所以ID是有值的
        itemMapper.insert(item);//将商品信息入库，会自动生成主键的id
        //获取商品详情
        ItemDesc itemDesc = itemVO.getItemDesc();
        itemDesc.setId(item.getId());//根据商品对象的主键id复制为商品详情的主键id
        itemDescMapper.insert(itemDesc);//将商品详情入库

    }

    @Override
    public void deleteItemById(Integer id) {
        itemMapper.deleteById(id);

    }

    @Override
    public void updateItem(Item item) {
        itemMapper.updateById(item);
    }


}
