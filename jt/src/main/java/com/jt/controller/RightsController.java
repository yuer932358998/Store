package com.jt.controller;

import com.jt.pojo.Rights;
import com.jt.pojo.User;
import com.jt.service.RightsService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin    //跨域资源共享
@RequestMapping("/rights")
public class RightsController {
    @Autowired
    private RightsService rightsService;




    /**
     * 查询左侧全新列表的数据要求查询两级列表的数据
     * URl： /rights/getRightsList
     * 参数：null
     * 返回值： SysResult对象
     * @return
     */
    @GetMapping("/getRightsList")
    public SysResult getRightsList(){
        List<Rights> list = rightsService.getRightsList();
        return SysResult.success(list);
    }

    @GetMapping("/list")    //测试是否能连接数据库
    public List<Rights> list(){
        return rightsService.list();
    }
}
